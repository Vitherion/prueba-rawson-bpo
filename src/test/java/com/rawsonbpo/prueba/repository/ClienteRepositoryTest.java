package com.rawsonbpo.prueba.repository;

import com.rawsonbpo.prueba.model.ClienteRequest;
import com.rawsonbpo.prueba.model.ClienteResponse;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ClienteRepositoryTest {
    
    @InjectMocks
    ClienteRepository clienteRepository;
    
    /**
     * Test of buscar method, of class ClienteRepository.
     */
    @Test
    public void testBuscar() {
        System.out.println("buscar");
        
        ClienteRequest clienteRequest = new ClienteRequest();
        clienteRequest.setNumeroDocumento("23445322");
        clienteRequest.setTipoDocumento("C");

        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setPrimerNombre("Ronal");
        clienteResponse.setSegundoNombre("Vito");
        clienteResponse.setPrimerApellido("Zarate");
        clienteResponse.setSegundoApellido("Motavita");
        clienteResponse.setTelefono("123456");
        clienteResponse.setDireccion("cra falsa 123");
        clienteResponse.setCiudadResidencia("Bogota D.C");
        
        ClienteResponse result = clienteRepository.buscar(clienteRequest);
        assertEquals(clienteResponse, result);
        
    }
    
}
