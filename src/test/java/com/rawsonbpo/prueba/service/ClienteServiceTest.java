package com.rawsonbpo.prueba.service;

import com.rawsonbpo.prueba.model.ClienteRequest;
import com.rawsonbpo.prueba.model.ClienteResponse;
import com.rawsonbpo.prueba.repository.ClienteRepository;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ClienteServiceTest {
    
    @InjectMocks
    ClienteService clienteService;

    @Mock
    ClienteRepository clienteRepository;
    
    /**
     * Test of buscar method, of class ClienteService.
     */
    @Test
    public void testBuscar() {
        System.out.println("buscar");
        
        ClienteRequest clienteRequest = new ClienteRequest();
        clienteRequest.setNumeroDocumento("23445322");
        clienteRequest.setTipoDocumento("C");

        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setPrimerNombre("Ronal");
        clienteResponse.setSegundoNombre("Vito");
        clienteResponse.setPrimerApellido("Zarate");
        clienteResponse.setSegundoApellido("Motavita");
        clienteResponse.setTelefono("123456");
        clienteResponse.setDireccion("cra falsa 123");
        clienteResponse.setCiudadResidencia("Bogota D.C");
        
        when(clienteRepository.buscar(clienteRequest)).thenReturn(clienteResponse);
        
        ClienteResponse result = clienteRepository.buscar(clienteRequest);
        assertNotNull(result);
        assertEquals(clienteResponse, result);
        
    }
    
}
