package com.rawsonbpo.prueba.controller;

import com.rawsonbpo.prueba.model.ClienteRequest;
import com.rawsonbpo.prueba.model.ClienteResponse;
import com.rawsonbpo.prueba.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    
    @Autowired
    private ClienteService clienteService;
    
    @GetMapping("/buscar")
    public ClienteResponse buscar(@RequestBody ClienteRequest clienteRequest) {
        
        return clienteService.buscar(clienteRequest);
    }
    
}
