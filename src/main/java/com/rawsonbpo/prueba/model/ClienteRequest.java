package com.rawsonbpo.prueba.model;

import lombok.Data;

@Data
public class ClienteRequest {
    
    private String numeroDocumento;
    private String tipoDocumento;
    
}
