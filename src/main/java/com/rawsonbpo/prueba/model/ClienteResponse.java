package com.rawsonbpo.prueba.model;

import lombok.Data;

@Data
public class ClienteResponse {
    
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String telefono;
    private String direccion;
    private String ciudadResidencia;
    
}
