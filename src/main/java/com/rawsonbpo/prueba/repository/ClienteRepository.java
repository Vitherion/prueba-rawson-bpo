package com.rawsonbpo.prueba.repository;

import com.rawsonbpo.prueba.model.ClienteRequest;
import com.rawsonbpo.prueba.model.ClienteResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class ClienteRepository {
    
    public ClienteResponse buscar(ClienteRequest clienteRequest) {
    
        ClienteResponse clienteResponse = new ClienteResponse();
        
        if(clienteRequest.getNumeroDocumento().equals("23445322")){
            clienteResponse.setPrimerNombre("Ronal");
            clienteResponse.setSegundoNombre("Vito");
            clienteResponse.setPrimerApellido("Zarate");
            clienteResponse.setSegundoApellido("Motavita");
            clienteResponse.setTelefono("123456");
            clienteResponse.setDireccion("cra falsa 123");
            clienteResponse.setCiudadResidencia("Bogota D.C");
        } else {
            log.info("cliente no encontrado "+clienteRequest.getNumeroDocumento());
        }

        return clienteResponse;
    }
    
}
