package com.rawsonbpo.prueba.service;

import com.rawsonbpo.prueba.model.ClienteRequest;
import com.rawsonbpo.prueba.model.ClienteResponse;
import com.rawsonbpo.prueba.repository.ClienteRepository;
import com.rawsonbpo.prueba.util.ComunUtil;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@Service
public class ClienteService {
    
    @Autowired
    ClienteRepository clienteRepository;

    public ClienteResponse buscar(ClienteRequest clienteRequest) {

        if (Objects.isNull(clienteRequest.getNumeroDocumento()) || clienteRequest.getNumeroDocumento().equals(""))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "el campo numeroDocumento es obligatorio");
        
        if (Objects.isNull(clienteRequest.getTipoDocumento()) || clienteRequest.getTipoDocumento().equals(""))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "el campo tipoDocumento es obligatorio");
        
        if(!clienteRequest.getTipoDocumento().equals("C") && !clienteRequest.getTipoDocumento().equals("P"))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "tipoDocumento no encontrado "+clienteRequest.getTipoDocumento());
        
        if(ComunUtil.isInvalidId(clienteRequest.getNumeroDocumento()))
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "numero de documento invalido");
        
        return clienteRepository.buscar(clienteRequest);
    }

}
