package com.rawsonbpo.prueba.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ComunUtil {
    
    public static boolean isInvalidId(String id){
        
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(id);
        
        return m.find();
    }
    
}
